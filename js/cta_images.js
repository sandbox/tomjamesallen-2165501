/**
 * @file
 * Handles loading of images and exposes external API.
 */

cta_images = (function () {

  // Plugin wide variables.
  var images_data = {},
      is_retina,
      device_resolution,
      images_on_page,
      allow_lazy_load = true,
      default_fade_in_time = 300,
      default_load_trigger = 'lazy_load';

  /**
   * Initialise plugin.
   */
  function init() {
    if (jQuery('.cta_images_noscript').length) {

      detect_retina();

      images_on_page = true;

      var $target_noscript_els = jQuery('.cta_images_noscript');

      $target_noscript_els.each(function (i) {

        // Get image.
        var $this_noscript_el = jQuery(this);

        // Set image id.
        $this_noscript_el.attr('data-image-id', i);

        // Get image srcs.
        var srcs = jQuery.parseJSON($this_noscript_el.attr('data-srcs'));

        // Calculate image width.
        var image_width = $this_noscript_el.parent().width();

        // Provides default values.
        var load_trigger = default_load_trigger;
        if ($this_noscript_el.attr('data-load-trigger') != undefined) {
          load_trigger = $this_noscript_el.attr('data-load-trigger');
        }
        var fade_time = default_fade_in_time;
        if ($this_noscript_el.attr('data-fade-time') != undefined) {
          fade_time = parseInt($this_noscript_el.attr('data-fade-time'));
        }
        var alt_attr = '';
        if ($this_noscript_el.attr('data-alt') != undefined) {
          alt_attr = $this_noscript_el.attr('data-alt');
        }

        // Calculate image size multipliers for current image.
        var multipliers = calc_multipliers($this_noscript_el);

        // Save image data attributes to image_data object.
        var image_data = {
          'id' : i,
          'noscript_el' : $this_noscript_el,
          'load_trigger' : load_trigger,
          'classes' : $this_noscript_el.attr('data-class'),
          'srcs' : srcs,
          'loaded' : false,
          'fade_time' : fade_time,
          'alt' : alt_attr,
          'non_retina_multiplier' : multipliers['non_retina_multiplier'],
          'retina_multiplier' : multipliers['retina_multiplier'],
        };

        // Save image_data object to images_data object, keyed by id.
        images_data[i] = image_data;

        // Add current src to image_data object. This has to be done after
        // initially saving 'image_data' to 'images_data', so that the
        // 'non_retina_multiplier' & 'retina_multiplier' image properties are
        // available to the 'calculate_src' function
        image_data['current_src'] = calculate_src(srcs, image_width, i);

        // Re-save 'image_data' object to 'images_data'.
        images_data[i] = image_data;

        // If load_trigger is 'document_ready', then add_image_to_dom.
        if (image_data['load_trigger'] === 'document_ready') {
          add_image_to_dom(image_data);
        }
      });

      // Bind Events.
      bind_events();
    }
  }

  /**
   * Calculate image size multipliers for current image.
   */
  function calc_multipliers($this_noscript_el) {

    // Calculate base multiplier
    var non_retina_multiplier;

    // If a data-attr is set on the <noscript> element, then set var to attr
    if ($this_noscript_el.attr('data-standard-display-resolution') != undefined) {
      non_retina_multiplier = $this_noscript_el.attr('data-standard-display-resolution');
    }

    // Data attr could be set to 'auto', or not set at all, in which case
    // set the default
    if (non_retina_multiplier == 'auto' || non_retina_multiplier == null) {
      non_retina_multiplier = 1;
    }

    // If the retina data-attr is set to anything other than 'auto', then
    // parse that value is a float multiply by the base multiplier.
    else {
      non_retina_multiplier = parseFloat(non_retina_multiplier);
    }

    // Calculate multiplier for high dpi displays
    var retina_multiplier;

    // If a data-attr is set on the <noscript> element, then set var to attr
    if ($this_noscript_el.attr('data-retina-display-resolution') != undefined) {
      retina_multiplier = $this_noscript_el.attr('data-retina-display-resolution');
    }

    // Data attr could be set to 'auto', or not set at all, in which case
    // set the default
    if (retina_multiplier == 'auto' || retina_multiplier == null) {

      // If device_resolution has been detected (could vary dependent on
      // browser), then set the retina_multiplier to the base
      // (non_retina_multiplier) resolution * the device_resolution, else
      // set to 1.5 * the base multiplier.
      if (device_resolution != undefined && device_resolution != null) {
        retina_multiplier = device_resolution * non_retina_multiplier;
      }
      else {
        retina_multiplier = 1.5 * non_retina_multiplier;
      }
    }

    // If the retina data-attr is set to anything other than 'auto', then
    // parse that value is a float multiply by the base multiplier.
    else {
      retina_multiplier = parseFloat(retina_multiplier) * non_retina_multiplier;
    }

    var multipliers = [];
    multipliers['non_retina_multiplier'] = non_retina_multiplier;
    multipliers['retina_multiplier'] = retina_multiplier;

    // Return multipliers array.
    return multipliers;
  }

  /**
   * Calculate the width of an image based on its id.
   */
  function calculate_image_width(image_id) {
    var $this_noscript_el = images_data[image_id]['noscript_el'];
    var image_width = $this_noscript_el.parent().width();
    return(image_width);
  }

  /**
   * Calculate optimum image src based on available srcs and the image's width
   */
  function calculate_src(srcs, image_width, image_id) {
    var image_width = image_width;
    non_retina_multiplier = images_data[image_id]['non_retina_multiplier'];
    retina_multiplier = images_data[image_id]['retina_multiplier'];
    if (is_retina === true) {
      image_width = image_width * retina_multiplier;
    }
    else {
      image_width = image_width * non_retina_multiplier;
    };
    var load_src = {};
    var max_load_src = {};
    var match_found = false;
    for (style_width in srcs) {
      delete max_load_src;
      max_load_src = {};
      max_load_src['src'] = srcs[style_width], 10;
      max_load_src['width'] = parseInt(style_width, 10);
      if (parseInt(style_width, 10) >= image_width) {
        match_found = true;
        delete load_src;
        load_src = {};
        load_src = max_load_src;
        break;
      }
    }
    if (match_found === false) {
      load_src = max_load_src;
    }
    return(load_src);
  }

  /**
   * Add new image to DOM.
   */
  function add_image_to_dom(image_data, fade_in, callback) {
    if (callback === undefined) {callback = (function () {});};
    if (fade_in === undefined) {fade_in = image_data['fade_time'];}
    // Create new image.
    var image = create_image(image_data);
    var $this_noscript_el = image_data['noscript_el'];
    // Render to DOM.
    $this_noscript_el.after(image);
    // Bind fade to load.
    jQuery('.cta_images_js_image').load(function () {
      jQuery(this).fadeIn(fade_in, function () {
        callback();
      });
    });
    // Set loaded value to true for image.
    var image_id = image_data['id'];
    images_data[image_id]['loaded'] = true;
  }

  /**
   * Creates and returns the markup for a new image.
   */
  function create_image(image_data) {
    var contruct_image
      = '<img ' +
      'class="cta_images_js_image ' + image_data['classes'] + '" ' +
      'src="' + image_data['current_src']['src'] + '" ' +
      'alt="' + image_data['alt'] + '" ' +
      'data-image-id="' + image_data['id'] + '" ' +
      'style="display: none;"' +
      '>';
    return(contruct_image);
  }

  /**
   * Swaps out an old image with a new one.
   */
  function swap_out_image(image_id, optimum_src, $old_image) {
    var clone_img = $old_image.clone();
    clone_img.attr({src: optimum_src['src']});
    update_current_src_data(image_id, optimum_src);
    clone_img.bind('load', function(){
      $old_image.replaceWith(clone_img).show(0);
    });
  }

  /**
   * Updates the current src property for a given image.
   */
  function update_current_src_data(image_id, src) {
    images_data[image_id]['current_src'] = src;
  }

  /**
   * Utility, triggered by init.
   *
   * Detects whether the display is high dpi, and returns a bool (is_retina) and
   * an integer (device_resolution).
   */
  function detect_retina() {
    is_retina = (
      window.devicePixelRatio > 1 ||
      (window.matchMedia && window.matchMedia("(-webkit-min-device-pixel-ratio: 1.5),(-moz-min-device-pixel-ratio: 1.5),(min-device-pixel-ratio: 1.5)").matches)
    );
    device_resolution = window.devicePixelRatio;
  }

  /**
   * Lazy load.
   */
  function lazy_load() {
    if (allow_lazy_load == true) {
      for (var image_id in images_data) {
        var image_data = images_data[image_id];
        if (image_data['load_trigger'] === 'lazy_load' && image_data['loaded'] == false) {
          if (lazy_load_logic(image_id) === true) {
            add_image_to_dom(image_data);
          }
        }
      }
    }
  }

  /**
   * Calculates whether an image is in view, and therefore whether it should be
   * loaded.
   */
  function lazy_load_logic(image_id) {
    var $this_noscript_el = images_data[image_id]['noscript_el'];
    var $this_noscript_el_parent = $this_noscript_el.parent();

    var windowHeight    = jQuery(window).height();
    var el              = $this_noscript_el_parent,
      offset          = el.offset(),
      scrollTop       = jQuery(window).scrollTop();

    // If the image is within half a screen's height of being in view then
    // return true.
    if ((scrollTop + (windowHeight * 1.5) > offset.top) && (scrollTop - windowHeight < offset.top + el.height())) {
      if ($this_noscript_el_parent.is(':visible')) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }

  /**
   * Bind events.
   *
   * Lazy loading must be bound to scroll events, and update_image_logic should
   * be re-triggered on resizing the window.
   */
  function bind_events() {
    lazy_load();
    jQuery(window).scroll(function () {
      lazy_load();
    });
    if (jQuery(window).smartresize) {
      jQuery(window).smartresize(function () {
        update_image_logic();
      });
    } else {
      jQuery(window).resize(function () {
        update_image_logic();
      });
    }
    
  }

  /**
   * Update image.
   *
   * Triggered on resizing the browser window. Also made available externally.
   *
   * Would need to be re-triggered manually after manually changing the size of
   * an image.
   */
  function update_image_logic() {
    for (image_id in images_data) {
      // Calculate image width.
      var image_width = calculate_image_width(image_id);
      // Get sources data for current image.
      var srcs = images_data[image_id]['srcs'];
      var optimum_src = calculate_src(srcs, image_width, image_id);
      var current_src = images_data[image_id]['current_src'];
      // Test for whether the js version of the image has been created.
      if (jQuery('.cta_images_js_image[data-image-id="' + image_id + '"]').length) {
        // Detect whether image should be swapped out with larger image.
        if (parseInt(optimum_src['width'], 10) > parseInt(current_src['width'], 10)) {
          var $this_image = jQuery('.cta_images_js_image[data-image-id="' + image_id + '"]');
          swap_out_image(image_id, optimum_src, $this_image);
        }
      }
      else {
        // Update the optimum source in the images_data array.
        update_current_src_data(image_id, optimum_src);
      }
    }
  }

  /**
   * Exposed function to load image from id.
   *
   * cta_images.external_load_trigger(image_id[, fade_time][, callback])
   *
   * If 'external_trigger' is selected as the load trigger, the loading of the
   * image can be triggered by calling cta_images.external_load_trigger(). The
   * function requires the id of the image to load as the first variable, and
   * can optionally be passed a fade time, and a callback function. Fade time
   * can be set to 'default'.
   *
   */
  function external_load_trigger(image_id, fade_in, callback) {
    if (images_on_page) {
      var image_data = images_data[image_id];
      // Handle defaults.
      if (callback === undefined) {callback = (function () {});};
      if (fade_in === undefined || fade_in === 'default') {fade_in = image_data['fade_time'];}

      if (image_data['loaded'] == false) {
        add_image_to_dom(image_data, fade_in, callback);
      }
      else {
        callback();
      }
    }
  }

  /**
   * Exposed function to block or allow lazy loading.
   *
   * allow_lazy_load_trigger(bool)
   *
   * Lazy loading is allowed by default. If 
   * cta_images.allow_lazy_load_trigger(false) is called externally, lazy
   * loading will be blocked until cta_images.allow_lazy_load_trigger(true) is
   * called. Calling cta_images.allow_lazy_load_trigger(true) will also trigger
   * a re-check of all images, and load any that are now in view.
   */
  function allow_lazy_load_trigger(bool) {
    if (bool === true) {
      allow_lazy_load = true;
      lazy_load();
    }
    else {
      allow_lazy_load = false;
    }
  }

  /**
   * Return exposed functions to make available externally.
   */
  return {
    init: init,
    allow_lazy_load_trigger: allow_lazy_load_trigger,
    external_load_trigger: external_load_trigger,
    update_image_logic: update_image_logic
  };
})();

jQuery(document).ready(function () {
  cta_images.init();
});
